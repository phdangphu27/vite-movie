import React from "react";
import Banner from "../../components/UI/Banner";
import Row from "../../components/UI/Row";
import { requests } from "../../Requests";
import Skeleton from "react-loading-skeleton";
import "./HomePage.css";
import LoadingScreen from "../../components/UI/LoadingScreen";

const HomePage = () => {
  const MOVIE = "movie";
  const TV = "tv";

  return (
    <div className='homePage'>
      <Banner apiEndpoint={requests.fetchTrending} type={MOVIE} />

      <div className='container p-4'>
        <Row
          apiEndpoint={requests.fetchPopularMovies}
          category={"Popular Movies"}
          type={MOVIE}
        />

        <Row
          apiEndpoint={requests.fetchNetflixOriginals}
          category={"Netflix Originals"}
          type={TV}
        />

        <Row
          apiEndpoint={requests.fetchDocumentaries}
          category={"Documentaries"}
          type={MOVIE}
        />

        <Row
          apiEndpoint={requests.fetchTopRated}
          category={"Top Rated Movies"}
          type={MOVIE}
        />

        <Row
          apiEndpoint={requests.fetchTopRatedTv}
          category={"Top Rated Series"}
          type={TV}
        />

        <Row
          apiEndpoint={requests.fetchRomanceMovies}
          category={"Romance"}
          type={MOVIE}
        />
      </div>
    </div>
  );
};

export default HomePage;
