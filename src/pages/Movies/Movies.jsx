import React from "react";
import { requests } from "../../Requests";
import Banner from "../../components/UI/Banner";
import MoviesGrid from "../../components/UI/MoviesGrid";
import "./Movies.css";
import { API_KEY } from "../../Requests";
import { BASE_URL } from "../../services/movieService";

const Movies = () => {
  return (
    <div className='movies'>
      <Banner apiEndpoint={requests.fetchPopularMovies} type={"movie"} />

      <div className='container mx-auto'>
        <MoviesGrid type={"movie"} apiEndpoint={requests.fetchPopularMovies} />
      </div>
    </div>
  );
};

export default Movies;
