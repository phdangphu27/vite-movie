import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MovieCard from "../../components/UI/MovieCard";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../../firestore";
import "./Favourites.css";
import { deleteFavourite } from "../../slice/movieSlice";
import { movieService } from "../../services/movieService";

const Favourites = () => {
  const currentUser = useSelector((state) => state.userSlice.user);
  const dispatch = useDispatch();
  const [favourites, setFavorites] = useState([]);

  useEffect(() => {
    fetchFavorites();
  }, [currentUser, dispatch, favourites]);

  const fetchFavorites = async () => {
    try {
      const docRef = doc(db, "favourites", currentUser?.uid);
      const favouriteSnapshot = await getDoc(docRef);
      if (favouriteSnapshot.exists()) {
        const favouriteData = favouriteSnapshot.data();
        const favList = favouriteData.favList;

        const favouriteMovies = await Promise.all(
          favList.map(async (favItem) => {
            const { type, id } = favItem;
            const movieData = await movieService.getDetails(id, type);
            return { ...movieData.data, type };
          })
        );

        setFavorites(favouriteMovies);
      }
    } catch (error) {
      console.error("Error fetching favorites:", error);
    }
  };

  const renderFavourites = () => {
    return (
      <div className='grid grid-cols-2 gap-3 2xl:grid-cols-4 xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2'>
        {favourites?.map((el) => {
          return (
            <div key={el.id}>
              <MovieCard movie={el} type={el.type} />
              <button
                onClick={() => {
                  const { id, type } = el;
                  const { uid } = currentUser;
                  dispatch(deleteFavourite({ uid, id, type }));
                }}
                className='bg-red-600 p-4 rounded w-full mt-3 hover:bg-red-700 transition-all'
              >
                REMOVE
              </button>
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className='favourites min-h-screen container mx-auto py-40'>
      {currentUser && (
        <div>
          <h3 className='text-2xl'>YOUR FAVOURITES</h3>
          {renderFavourites()}
        </div>
      )}
    </div>
  );
};

export default Favourites;
