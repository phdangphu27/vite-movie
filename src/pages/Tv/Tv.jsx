import React from "react";
import { requests } from "../../Requests";
import Banner from "../../components/UI/Banner";
import MoviesGrid from "../../components/UI/MoviesGrid";
import { API_KEY } from "../../Requests";

const Tv = () => {
  return (
    <div>
      <Banner apiEndpoint={requests.fetchTopRatedTv} type={"tv"} />

      <div className='container mx-auto'>
        <MoviesGrid
          type={"tv"}
          apiEndpoint={`/tv/top_rated?api_key=${API_KEY}`}
        />
      </div>
    </div>
  );
};

export default Tv;
