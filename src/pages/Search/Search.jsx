import React, { useEffect, useState } from "react";
import MoviesGrid from "../../components/UI/MoviesGrid";
import { API_KEY } from "../../Requests";

const Search = () => {
  const [activeButton, setActiveButton] = useState(1);
  const [type, setType] = useState("movie");
  const [input, setInput] = useState("");
  const [debouncedInput, setDebouncedInput] = useState("");
  const apiEndpoint = `/search/${type}?api_key=${API_KEY}&language=en-US&query=${input}`;
  const [renderKey, setRenderKey] = useState(0);

  const handleClickActive = (id) => {
    setActiveButton(id);
    setRenderKey((prevKey) => prevKey + 1);
  };

  const handleInputChange = (event) => {
    setInput(event.target.value);
    setRenderKey((prevKey) => prevKey + 1);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedInput(input);
    }, 500);
    return () => {
      clearTimeout(timer);
    };
  }, [debouncedInput, input]);

  return (
    <div className='container mx-auto text-white min-h-screen pt-12'>
      <div className='pt-28 flex flex-col'>
        <div className='flex mx-auto'>
          <button
            className={`${activeButton === 1 ? "active" : ""} nav__button`}
            onClick={() => {
              setType("movie");
              handleClickActive(1);
            }}
          >
            MOVIES
          </button>
          <button
            className={`${activeButton === 2 ? "active" : ""} nav__button`}
            onClick={() => {
              setType("tv");
              handleClickActive(2);
            }}
          >
            TV SERIES
          </button>
          <button
            className={`${activeButton === 3 ? "active" : ""} nav__button`}
            onClick={() => {
              setType("person");
              handleClickActive(3);
            }}
          >
            PEOPLE
          </button>
        </div>
        <form>
          <input
            name='input'
            id='input'
            type='text'
            onChange={handleInputChange}
            value={input}
            placeholder='Search'
            className='w-full rounded p-3 bg-black border-2 mt-4'
          />
        </form>
      </div>
      {debouncedInput && (
        <MoviesGrid key={renderKey} type={type} apiEndpoint={apiEndpoint} />
      )}
    </div>
  );
};

export default Search;
