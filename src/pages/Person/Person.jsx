import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import MovieCard from "../../components/UI/MovieCard";
import moment from "moment";
import { useMediaQuery } from "react-responsive";
import "./Person.css";
import useApi from "../../hook/useApi";

const Person = () => {
  const { id } = useParams();
  const [person, setPerson] = useState(null);
  const [media, setMedia] = useState([]);
  const isMobile = useMediaQuery({ minWidth: 320, maxWidth: 1024 });
  const url = `/person/${id}/combined_credits`;
  const personUrl = `/person/${id}`;
  const { data } = useApi(url);
  const { data: personData } = useApi(personUrl);
  const backgroundImage = `https://image.tmdb.org/t/p/w500/${person?.profile_path}`;

  useEffect(() => {
    if (data?.cast && personData) {
      setMedia(data?.cast);
      setPerson(personData);
    }
  }, [data, personData]);

  return (
    <div className='text-white container mx-auto pt-40 px-4 min-h-screen'>
      <div className='personPage__header flex 2xl:flex-row xl:flex-row lg:flex-row md:flex-col sm:flex-col sm:items-center'>
        <img
          className='p-3'
          style={{ minWidth: "250px", maxWidth: "350px" }}
          src={backgroundImage}
        />
        <div className='text-md pl-4 font-bold text-gray-300'>
          {!isMobile
            ? person?.biography.length > 1500
              ? person?.biography.slice(0, 1400) + "..."
              : person?.biography
            : person?.biography.slice(0, 250) + "..."}
        </div>
      </div>

      <div className='flex flex-col my-12'>
        <div className='grid grid-cols-2 gap-3 2xl:grid-cols-4 xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2'>
          {media
            ?.sort((a, b) => {
              const dateA = a.release_date || a.first_air_date;
              const dateB = b.release_date || b.first_air_date;
              return moment(dateB).diff(moment(dateA));
            })
            .map((item) => {
              {
                return item.poster_path ? (
                  <MovieCard movie={item} type={item.media_type} />
                ) : (
                  ""
                );
              }
            })}
        </div>
      </div>
    </div>
  );
};

export default Person;
