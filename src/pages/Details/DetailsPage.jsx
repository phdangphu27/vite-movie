import React, { useEffect, useState, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import { Progress, Slider } from "antd";
import { Swiper, SwiperSlide } from "swiper/react";
import Tag from "../../components/UI/Tag";
import { Pagination, Navigation } from "swiper";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css";
import { useDispatch, useSelector } from "react-redux";
import Row from "../../components/UI/Row";
import ReviewsForm from "../../components/form/ReviewsForm";
import PreviousComments from "../../components/UI/PreviousComments";
import { setShow } from "../../slice/userSlice";
import "./DetailsPage.css";
import useApi from "../../hook/useApi";
import { API_KEY } from "../../Requests";
import { addFavourite } from "../../slice/movieSlice";

const DetailsPage = () => {
  const targetRef = useRef(null);
  const { id, type } = useParams();
  const [genresMovie, setGenresMovie] = useState([]);
  const [movie, setMovie] = useState([]);
  const [casts, setCasts] = useState([]);
  const currentUser = useSelector((state) => state.userSlice.user);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const url = `/${type}/${id}?api_key=${API_KEY}&append_to_response=videos,credits`;
  const { data } = useApi(url);

  const handleClick = () => {
    targetRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    if (data) {
      setMovie(data);
      setCasts(data?.credits.cast);
    }
  }, [data]);

  return (
    <div className='text-white'>
      <div
        className='detailsPage__backdrop bg-no-repeat bg-cover bg-top'
        style={{
          backgroundImage: `url(${`https://image.tmdb.org/t/p/original/${movie?.backdrop_path}`})`,
        }}
      >
        <div className='detailsPage__backdrop--faded'></div>
      </div>

      <div className='p-4 container mx-auto'>
        <div className='detailsPage__header flex -mt-80 2xl:flex-row xl:flex-row lg:flex-row md:flex-col sm:flex-col sm:items-center'>
          <img
            className='p-10'
            src={`https://image.tmdb.org/t/p/w500/${movie?.poster_path}`}
          />
          <div className='detailsPage__contents pl-4'>
            <div className='detailsPage__info'>
              <h1 className='text-6xl mt-5'>
                {(movie?.original_title || movie?.original_name) + " "}
                {movie.release_date?.slice(0, 4) ||
                  movie.first_air_date?.slice(0, 4)}
              </h1>
              <div className='detailsPage__rate-genres py-12'>
                <Progress
                  type='circle'
                  percent={movie?.vote_average * 10}
                  format={(percent) => (
                    <div style={{ color: "white" }}>
                      {(percent / 10).toFixed(1)}
                    </div>
                  )}
                  width={50}
                  strokeColor='green'
                  strokeWidth={10}
                />

                {movie.genres?.map((el) => {
                  return <Tag>{el.name}</Tag>;
                })}
              </div>
              <div className='detailsPage__overview font-medium'>
                {movie?.overview}
              </div>
            </div>
            <div className='py-12'>
              <button
                className='detailsPage__button bg-gray-600 text-white font-bold py-2 px-4 rounded'
                onClick={() => {
                  !currentUser
                    ? dispatch(setShow(true))
                    : dispatch(
                        addFavourite({ type, id, uid: currentUser?.uid })
                      );
                }}
              >
                + My List
              </button>
              <button
                className='detailsPage__button 
                bg-gray-600
                text-white
                font-bold
                py-2
                px-4
                rounded'
                onClick={handleClick}
              >
                Trailer
              </button>
            </div>
            <div className='detailsPage__casts'>
              <h2 className='text-2xl pb-5'>CASTS</h2>
              <Swiper
                slidesPerView={2}
                spaceBetween={10}
                breakpoints={{
                  600: {
                    slidesPerView: 2,
                  },
                  768: {
                    slidesPerView: 3,
                  },
                  1024: {
                    slidesPerView: 4,
                  },
                  1280: {
                    slidesPerView: 5,
                  },
                }}
              >
                {casts?.map((cast) => {
                  const castImage = cast.profile_path
                    ? `url(https://image.tmdb.org/t/p/original${cast?.profile_path})`
                    : "url(https://cdn.madisonds.com.au/home/wp-content/uploads/Grey-blank-panel.png)";

                  return (
                    <SwiperSlide
                      className='detailsPage__castImage cursor-pointer bg-cover bg-center bg-no-repeat'
                      onClick={() => {
                        navigate(`/person/${cast.id}`);
                      }}
                      style={{
                        backgroundImage: castImage,
                      }}
                    >
                      <div
                        style={{ backgroundColor: "#7c7c7ca3" }}
                        className='w-full p-3 text-center text-sm absolute bottom-0 left-0'
                      >
                        {cast?.name.length > 15
                          ? cast.name.slice(0, 12) + "..."
                          : cast.name}
                      </div>
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </div>
          </div>
        </div>
        <div className='py-12' ref={targetRef}>
          <h2 className='text-2xl py-4'>VIDEOS</h2>
          <Swiper
            modules={[Pagination, Navigation]}
            onSlideChange={() => {}}
            pagination={{ clickable: true }}
            navigation
            onSwiper={() => {}}
            spaceBetween={20}
          >
            {movie.videos?.results.slice(0, 4).map((video) => {
              return (
                <SwiperSlide key={video.key}>
                  <iframe
                    className='video'
                    width='100%'
                    height='500px'
                    src={`https://www.youtube.com/embed/${video?.key}`}
                    title='YouTube video player'
                    frameBorder='0'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share'
                    allowFullScreen
                    slideperview={2}
                  ></iframe>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
        <div className='detailsPage__reviews'>
          <h2 className='text-2xl'>REVIEWS</h2>
          <PreviousComments type={type} id={id} />
          {currentUser && <ReviewsForm type={type} id={id} />}
        </div>
        <div className='detailsPage__recommendations py-4'>
          <Row
            apiEndpoint={`/${type}/${id}/recommendations`}
            type={type}
            category={"Recommendations"}
          />
        </div>
      </div>
    </div>
  );
};

export default DetailsPage;
