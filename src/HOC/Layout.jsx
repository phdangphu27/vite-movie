import React, { useDebugValue } from "react";
import Nav from "../components/UI/Nav";
import Footer from "../components/UI/Footer";
import "./Layout.css";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useDispatch, useSelector } from "react-redux";
import "react-loading-skeleton/dist/skeleton.css";
import LoadingScreen from "../components/UI/LoadingScreen";
import { useLocation } from "react-router-dom";
import { resetMovieMessageAndError } from "../slice/movieSlice";
import { resetUserMessageAndError } from "../slice/userSlice";
import { useEffect } from "react";

const Layout = ({ children }) => {
  const movieMessage = useSelector((state) => state.movieSlice.message);
  const userMessage = useSelector((state) => state.userSlice.message);
  const error = useSelector((state) => state.userSlice.error);
  const loading = useSelector((state) => state.userSlice.loading);
  const dataLoading = useSelector((state) => state.movieSlice.loading);
  const dispatch = useDispatch();
  const location = useLocation();
  const isLoading = loading || dataLoading;

  console.log(error);
  console.log(userMessage, movieMessage);

  useEffect(() => {
    dispatch(resetMovieMessageAndError());
    dispatch(resetUserMessageAndError());
  }, [movieMessage, userMessage, location, error]);

  const notifyError = (error) => {
    toast.error(error, {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "dark",
    });
  };

  const notifySuccess = (message) => {
    toast.success(message, {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "dark",
    });
  };

  return (
    <div className='layout'>
      {movieMessage && notifySuccess(movieMessage)}
      {userMessage && notifySuccess(userMessage)}
      {error && notifyError(error)}
      {isLoading && <LoadingScreen />}
      {!loading && (
        <>
          <Nav />
          {children}
          <Footer />
          <ToastContainer
            position='bottom-left'
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme='dark'
          />
        </>
      )}
    </div>
  );
};

export default Layout;
