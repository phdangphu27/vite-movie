import { useState, useEffect } from "react";
import { movieService } from "../services/movieService";
import { setError } from "../slice/userSlice";
import { useDispatch } from "react-redux";
import { setLoading } from "../slice/movieSlice";

const useApi = (url, page) => {
  const [data, setData] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      dispatch(setLoading(true));

      try {
        const response = await movieService.getMoviesData(url, page);
        setData(response.data);
      } catch (error) {
        console.log(error);
        dispatch(setError(error));
      }

      const timeOut = setTimeout(() => {
        dispatch(setLoading(false));
      }, 1000);

      return () => timeOut();
    };

    fetchData();
  }, [url, page]);

  return { data };
};

export default useApi;
