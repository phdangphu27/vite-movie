import { createAsyncThunk } from "@reduxjs/toolkit";
import { movieService } from "../services/movieService";

export const fetchMoviesData = createAsyncThunk(
  "fetchMoviesData",
  async (url) => {
    try {
      const request = await movieService.getMoviesData(url);
      console.log(request.data.results);
      return request.data.results;
    } catch (error) {
      alert(error);
    }
  }
);
