import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/Home/HomePage";
import ProfilePage from "./pages/Profile/ProfilePage";
import "./App.css";
import Layout from "./HOC/Layout";
import Movies from "../src/pages/Movies/Movies";
import DetailsPage from "./pages/Details/DetailsPage";
import Favourites from "./pages/Favourites/Favourites";
import Tv from "./pages/Tv/Tv";
import Search from "./pages/Search/Search";
import NotFound from "./pages/NotFound/NotFound";
import Person from "./pages/Person/Person";
import ScrollToTop from "./hook/scrollToTop";

function App() {
  return (
    <div className='app'>
      <BrowserRouter>
        <ScrollToTop />
        <Routes>
          <Route
            path='/'
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path='/home'
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path='/movies'
            element={
              <Layout>
                <Movies />
              </Layout>
            }
          />
          <Route
            path='/tv'
            element={
              <Layout>
                <Tv />
              </Layout>
            }
          />
          <Route
            path='/profile'
            element={
              <Layout>
                <ProfilePage />
              </Layout>
            }
          />
          <Route
            path='/:type/:id'
            element={
              <Layout>
                <DetailsPage />
              </Layout>
            }
          />
          <Route
            path='/favourites'
            element={
              <Layout>
                <Favourites />
              </Layout>
            }
          />
          <Route
            path='/search'
            element={
              <Layout>
                <Search />
              </Layout>
            }
          />
          <Route
            path='/person/:id'
            element={
              <Layout>
                <Person />
              </Layout>
            }
          />
          <Route path='*' element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
