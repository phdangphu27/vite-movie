export const API_KEY = "180bb364818aa2e676bf9d277ecd25e9";

export const requests = {
  fetchTrending: `/trending/movie/week`,
  fetchNetflixOriginals: `/discover/tv?api_key=${API_KEY}&with_networks=213`,
  fetchActionMovies: `/discover/movie?api_key=${API_KEY}&with_genres=28`,
  fetchComedyMovies: `/discover/movie?api_key=${API_KEY}&with_genres=35`,
  fetchHorrorMovies: `/discover/movie?api_key=${API_KEY}&with_genres=27`,
  fetchRomanceMovies: `/discover/movie?api_key=${API_KEY}&with_genres=10749`,
  fetchGenresMovie: `/genre/movie/list`,
  fetchGerensTv: `/genre/tv/list`,
  fetchTopRated: `/movie/top_rated`,
  fetchDocumentaries: `/discover/movie?api_key=${API_KEY}&with_genres=99`,
  fetchPopularMovies: `/movie/popular`,
  fetchPopularTv: `/tv/popular?api_key=${API_KEY}&language=en-US&page=1`,
  fetchTopRatedTv: `/tv/top_rated?api_key=${API_KEY}&language=en-US&page=1`,
};
