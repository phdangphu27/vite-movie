import { configureStore } from "@reduxjs/toolkit";
import userSlice from "../slice/userSlice";
import movieSlice from "../slice/movieSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    movieSlice,
  },
});
