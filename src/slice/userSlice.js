import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { auth } from "../firestore";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  updateProfile,
  updatePassword,
  EmailAuthProvider,
  reauthenticateWithCredential,
  sendPasswordResetEmail,
} from "firebase/auth";
import { signOut } from "firebase/auth";

const initialState = {
  user: null,
  show: false,
  updateShow: false,
  error: null,
  favList: [],
  loading: false,
};

export const signIn = createAsyncThunk(
  "auth/signIn",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const { user } = await signInWithEmailAndPassword(auth, email, password);
      return user;
    } catch (error) {
      return rejectWithValue(error.code);
    }
  }
);

export const signUp = createAsyncThunk(
  "auth/signUp",
  async ({ email, password, displayName }, { rejectWithValue }) => {
    try {
      const { user } = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      await updateProfile(user, {
        displayName,
      });
      return user;
    } catch (error) {
      return rejectWithValue(error.code);
    }
  }
);

export const logout = createAsyncThunk("auth/signout", async () => {
  try {
    signOut(auth);
    return true;
  } catch (error) {
    console.log(error);
  }
});

export const updateUserPassword = createAsyncThunk(
  "auth/updatePassword",
  async ({ currentPassword, newPassword }) => {
    const user = auth.currentUser;
    try {
      const credential = EmailAuthProvider.credential(
        user.email,
        currentPassword
      );
      await reauthenticateWithCredential(user, credential);
      updatePassword(user, newPassword);
      console.log("Password updated successfully!");
      return user;
    } catch (error) {
      return rejectWithValue(error.code);
    }
  }
);

export const resetPassword = createAsyncThunk(
  "auth/reset",
  async (email, { rejectWithValue }) => {
    try {
      const res = await sendPasswordResetEmail(auth, email);
      console.log(res);
      return res;
    } catch (error) {
      return rejectWithValue(error.code);
    }
  }
);

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setCurrentUser: (state, action) => {
      state.user = action.payload;
    },
    setUpdateShow: (state, action) => {
      state.updateShow = action.payload;
    },
    setShow: (state, action) => {
      state.show = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    resetUserMessageAndError: (state, action) => {
      state.message = null;
      state.error = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(signIn.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(signIn.fulfilled, (state, action) => {
        state.loading = false;
        state.user = {
          email: action.payload.email,
          uid: action.payload.uid,
          displayName: action.payload.displayName,
        };
        state.show = false;
        state.error = null;
        state.message = "Signed in";
      })
      .addCase(signIn.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      })
      .addCase(signUp.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(signUp.fulfilled, (state, action) => {
        state.loading = false;
        state.user = {
          email: action.payload.email,
          uid: action.payload.uid,
          displayName: action.payload.displayName,
        };
        state.show = false;
        state.error = null;
        state.message = "Sign in success";
      })
      .addCase(signUp.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
        state.message = "";
        console.log(action.payload);
      })
      .addCase(logout.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(logout.fulfilled, (state, action) => {
        state.loading = false;
        state.show = false;
        state.error = null;
        state.message = "";
      })
      .addCase(logout.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
        state.message = "";
      })
      .addCase(updateUserPassword.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateUserPassword.fulfilled, (state, action) => {
        state.loading = false;
        state.user = {
          email: action.payload.email,
          uid: action.payload.uid,
          displayName: action.payload.displayName,
        };
        state.updateShow = false;
        state.error = null;
        state.message = "Password updated";
      })
      .addCase(updateUserPassword.rejected, (state, action) => {
        state.loading = false;
        state.error = "Wrong password";
      })
      .addCase(resetPassword.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(resetPassword.fulfilled, (state, action) => {
        state.loading = false;
        state.updateShow = false;
        state.error = null;
        state.message = "Check your email";
      })
      .addCase(resetPassword.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      });
  },
});

export const {
  setCurrentUser,
  setShow,
  setError,
  setUpdateShow,
  resetUserMessageAndError,
} = userSlice.actions;

export default userSlice.reducer;
