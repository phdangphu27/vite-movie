import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { movieService } from "../services/movieService";
import {
  updateDoc,
  doc,
  arrayRemove,
  getDoc,
  setDoc,
  arrayUnion,
} from "firebase/firestore";
import { db } from "../firestore";

const initialState = {
  loading: false,
  message: null,
};

export const deleteComment = createAsyncThunk(
  "deleteComment",
  async ({ cid, type, id }, { rejectWithValue }) => {
    const docRef = doc(db, "comments", type);
    const docData = await getDoc(docRef);

    const updatedComments = docData.data()[`${id}`].filter((item) => {
      return item.cid !== String(cid);
    });

    try {
      await updateDoc(docRef, { [`${id}`]: updatedComments });
    } catch (error) {
      console.log("Error deleting comment: ", error);
    }
  }
);

export const addFavourite = createAsyncThunk(
  "addFavourites",
  async ({ type, id, uid }) => {
    const docRef = doc(db, "favourites", uid);

    try {
      const respone = await setDoc(
        docRef,
        {
          [`favList`]: arrayUnion({
            id,
            type,
          }),
        },
        { merge: true }
      );
      return respone;
    } catch (error) {
      console.log(error);
    }
  }
);

export const deleteFavourite = createAsyncThunk(
  "deleteFavourites",
  async ({ uid, id, type }) => {
    const docRef = doc(db, "favourites", uid);
    const docData = await getDoc(docRef);

    const updatedFavList = docData.data()[`favList`].filter((item) => {
      return item.id !== String(id) || item.type !== type;
    });

    try {
      await updateDoc(docRef, { favList: updatedFavList });
    } catch (error) {
      console.log(error.message);
    }
  }
);

const movieSlice = createSlice({
  name: "movieSlice",
  initialState,
  reducers: {
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    resetMovieMessageAndError: (state, action) => {
      state.message = null;
      state.error = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(deleteComment.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(deleteComment.fulfilled, (state, action) => {
        state.loading = false;
      })
      .addCase(deleteComment.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(deleteFavourite.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(deleteFavourite.fulfilled, (state, action) => {
        state.loading = false;
        state.message = "Removed";
      })
      .addCase(deleteFavourite.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(addFavourite.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(addFavourite.fulfilled, (state, action) => {
        state.loading = false;
        state.message = "Added";
      })
      .addCase(addFavourite.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        state.message = "Already added";
      });
  },
});

export const { setLoading, resetMovieMessageAndError } = movieSlice.actions;

export default movieSlice.reducer;
