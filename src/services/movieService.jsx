import axios from "axios";
import { API_KEY } from "../Requests";

export const BASE_URL = "https://api.themoviedb.org/3";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  params: {
    api_key: API_KEY,
    language: "en-US",
  },
});

export const movieService = {
  getMoviesData: (fetchUrl, page = 1) => {
    return axiosInstance.get(fetchUrl, { params: { page } });
  },

  getDetails: (id, type) => {
    return axiosInstance.get(
      `/${type}/${id}?api_key=${API_KEY}&append_to_response=videos,credits`
    );
  },
};
