import React, { useEffect } from "react";
import { useState } from "react";
import MovieCard from "./MovieCard";
import PersonCard from "./PersonCard";
import useApi from "../../hook/useApi";

const MoviesGrid = ({ type, apiEndpoint }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [media, setMedia] = useState([]);
  const { data } = useApi(apiEndpoint, currentPage);

  useEffect(() => {
    if (data) {
      setMedia((prevData) => [...prevData, ...data.results]);
    }
  }, [data]);

  const renderGrid = () => {
    return media.map((item) => {
      return type === "person" ? (
        <div key={item.id}>
          <PersonCard person={item} />
        </div>
      ) : (
        <>
          {item.poster_path ? (
            <MovieCard movie={item} type={type ? type : item.media_type} />
          ) : (
            ""
          )}
        </>
      );
    });
  };

  return (
    <div className='flex flex-col justify-center p-4'>
      <div className='grid grid-cols-2 gap-3 2xl:grid-cols-4 xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2'>
        {renderGrid()}
      </div>
      <div className='flex justify-center bg-black py-5'>
        <button
          className='text-red-600 bg-transparent border-none hover:cursor-pointer'
          onClick={() => {
            setCurrentPage((prev) => prev + 1);
          }}
        >
          LOAD MORE
        </button>
      </div>
    </div>
  );
};

export default MoviesGrid;
