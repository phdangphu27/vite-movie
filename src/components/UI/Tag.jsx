import React from "react";

const Tag = ({ children }) => {
  return (
    <span
      style={{ borderRadius: "24px" }}
      className='ml-3 p-2 bg-red-600 text-sm'
    >
      {children}
    </span>
  );
};

export default Tag;
