import React, { useState } from "react";
import "./UserMenu.css";
import { Menu, Dropdown, Modal } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { auth } from "../../firestore";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "firebase/auth";
import {
  setUpdateShow,
  updateUserPassword,
  setError,
} from "../../slice/userSlice";
import { logout } from "../../slice/userSlice";

const UserMenu = ({ name }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const updateShow = useSelector((state) => state.userSlice.updateShow);
  const updateError = useSelector((state) => state.userSlice.error);

  const menu = (
    <Menu>
      <Menu.Item
        key='1'
        onClick={() => {
          navigate("/Favourites");
        }}
      >
        My List
      </Menu.Item>
      <Menu.Item
        onClick={() => {
          dispatch(setUpdateShow(true));
        }}
        key='2'
      >
        Update Password
      </Menu.Item>
      <Menu.Item
        key='3'
        onClick={() => {
          dispatch(logout());
        }}
      >
        Logout
      </Menu.Item>
    </Menu>
  );

  const passwordUpdateSchema = Yup.object().shape({
    currentPassword: Yup.string()
      .required("Current password is required")
      .min(6, "New password must be at least 6 characters long"),
    newPassword: Yup.string()
      .required("New password is required")
      .min(6, "New password must be at least 6 characters long"),
    confirmPassword: Yup.string()
      .required("Confirm password is required")
      .oneOf([Yup.ref("newPassword"), null], "Passwords must match"),
  });

  const passwordFormik = useFormik({
    initialValues: {
      currentPassword: "",
      newPassword: "",
      confirmPassword: "",
    },

    validationSchema: passwordUpdateSchema,

    onSubmit: ({ currentPassword, newPassword }) => {
      dispatch(updateUserPassword({ currentPassword, newPassword }));
    },
  });

  return (
    <>
      <Modal
        open={updateShow}
        onCancel={() => {
          dispatch(setUpdateShow(false));
          dispatch(setError(""));
          passwordFormik.resetForm();
        }}
        onOk={() => dispatch(setUpdateShow(false))}
        footer={null}
        className='updateModal'
      >
        <h3>UPDATE PASSWORD</h3>
        <form
          onSubmit={passwordFormik.handleSubmit}
          className='passwordUpdateForm'
        >
          <div className='formItem'>
            {passwordFormik.touched.currentPassword &&
            passwordFormik.errors.currentPassword ? (
              <div style={{ color: "red" }}>
                {passwordFormik.errors.currentPassword}
              </div>
            ) : (
              <div>Current Password</div>
            )}
            <input
              id='currentPassword'
              name='currentPassword'
              type='password'
              onChange={passwordFormik.handleChange}
              onBlur={passwordFormik.handleBlur}
              value={passwordFormik.values.currentPassword}
            />
          </div>
          <div className='formItem'>
            {passwordFormik.touched.newPassword &&
            passwordFormik.errors.newPassword ? (
              <div style={{ color: "red" }}>
                {passwordFormik.errors.newPassword}
              </div>
            ) : (
              <div>New Password</div>
            )}
            <input
              id='newPassword'
              name='newPassword'
              type='password'
              onChange={passwordFormik.handleChange}
              onBlur={passwordFormik.handleBlur}
              value={passwordFormik.values.newPassword}
            />
          </div>
          <div className='formItem'>
            {passwordFormik.touched.confirmPassword &&
            passwordFormik.errors.confirmPassword ? (
              <div style={{ color: "red" }}>
                {passwordFormik.errors.confirmPassword}
              </div>
            ) : (
              <div>Confirm Password</div>
            )}
            <input
              id='confirmPassword'
              name='confirmPassword'
              type='password'
              onChange={passwordFormik.handleChange}
              onBlur={passwordFormik.handleBlur}
              value={passwordFormik.values.confirmPassword}
            />
          </div>
          <button
            type='submit'
            className='bg-red-600 rounded w-full p-4 mt-3 hover:bg-red-800'
          >
            Update
          </button>
        </form>
        {updateError && <div className='modalError'>{"❗" + updateError}</div>}
      </Modal>
      <Dropdown overlay={menu}>
        <a className='ant-dropdown-link' onClick={(e) => e.preventDefault()}>
          <UserOutlined /> {name}
        </a>
      </Dropdown>
    </>
  );
};

export default UserMenu;
