import React from "react";
import { useNavigate, useParams } from "react-router-dom";

const PersonCard = ({ person }) => {
  const navigate = useNavigate();

  const backgroundImage = person.profile_path
    ? `url('https://image.tmdb.org/t/p/w500/${person?.profile_path}')`
    : "url('https://cdn.madisonds.com.au/home/wp-content/uploads/Grey-blank-panel.png')";

  return (
    <div
      onClick={() => {
        navigate(`/person/${person.id}`);
      }}
      className='justify-center card__poster cursor-pointer relative bg-cover bg-no-repeat bg-center'
      style={{
        backgroundImage: backgroundImage,
        paddingTop: "160%",
      }}
    >
      <div className='absolute bottom-0 left-0 text-center bg-gray-500 p-3 w-full'>
        {person.name}
      </div>
    </div>
  );
};

export default PersonCard;
