import React from "react";
import { Progress } from "antd";
import { useNavigate } from "react-router-dom";
import "./MovieCard.css";
import { useMediaQuery } from "react-responsive";

const MovieCard = ({ movie, type }) => {
  const navigate = useNavigate();
  const isMobile = useMediaQuery({ minWidth: 320, maxWidth: 767 });

  const backgroundImage = `url(https://image.tmdb.org/t/p/w500/${movie.poster_path})`;

  return (
    <div
      className='card__poster text-white relative bg-cover bg-center bg-no-repeat'
      style={{
        backgroundImage: backgroundImage,
        paddingTop: "75%",
        paddingBottom: "75%",
      }}
    >
      <div
        className='card__poster--faded cursor-pointer inset-0 absolute h-full'
        onClick={() => {
          navigate(`/${type}/${movie?.id}`);
        }}
      >
        <div className='px-3 pb-3 absolute inline-block bottom-0'>
          <Progress
            type='circle'
            percent={movie.vote_average * 10}
            format={(percent) => (
              <div style={{ color: "white" }}>{(percent / 10).toFixed(1)}</div>
            )}
            width={50}
            strokeColor='green'
            strokeWidth={10}
          />
          <h2 className='mt-2 text-md'>
            {movie.first_air_date?.slice(0, 4) ||
              movie.release_date?.slice(0, 4)}
          </h2>
          <h2 className='mt-2 text-md'>
            {isMobile
              ? movie.original_name?.slice(0, 16) ||
                movie.original_title?.slice(0, 16) + "..."
              : movie.original_name || movie.original_title}
          </h2>
        </div>
      </div>
    </div>
  );
};

export default MovieCard;
