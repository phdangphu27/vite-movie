import React, { useEffect, useState } from "react";
import { doc, onSnapshot } from "firebase/firestore";
import { db } from "../../firestore";
import "./PreviousComments.css";
import { useDispatch, useSelector } from "react-redux";
import { deleteComment } from "../../slice/movieSlice";

const PreviousComments = ({ type, id }) => {
  const [comments, setComments] = useState([]);
  const currentUser = useSelector((state) => state.userSlice.user);
  const dispatch = useDispatch();
  const docRef = doc(db, "comments", type);

  useEffect(() => {
    const unsub = onSnapshot(docRef, (doc) => {
      const data = doc.data()[`${id}`];
      setComments(data);
    });
    return () => unsub();
  }, [type, id]);

  const renderPreviousComments = () => {
    return comments?.map((comment) => {
      return (
        <div
          key={comment.cid}
          className='previousComments_box p-4 rounded flex justify-between items-center transition-all duration-200'
        >
          <div className='mb-3 flex items-center'>
            <div className='previousComments_userAvatar flex justify-center rounded-full w-12 h12 bg-blue-600'>
              {comment.displayName?.slice(0, 1).toUpperCase()}
            </div>
            <div className='ml-4'>
              <h3 className='mb-3 text-xl'>{comment.displayName}</h3>
              <div className='mb-3 text-lg'>{comment.createdAt}</div>
              <p className='text-lg'>{comment.content}</p>
            </div>
          </div>
          <div>
            {currentUser && currentUser?.uid === comment.uid ? (
              <button
                onClick={() => {
                  dispatch(deleteComment({ cid: comment.cid, type, id }));
                }}
                className='bg-red-600 rounded p-4'
              >
                REMOVE
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
      );
    });
  };

  return <div className='py-4'>{renderPreviousComments()}</div>;
};

export default PreviousComments;
