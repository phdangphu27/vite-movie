import React, { useState } from "react";
import { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import SignUpModal from "../form/SignUpModal";
import { useDispatch, useSelector } from "react-redux";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../../firestore";
import { setCurrentUser } from "../../slice/userSlice";
import UserMenu from "./UserMenu";
import { useMediaQuery } from "react-responsive";
import Hamburger from "./Hamburger";
import "./Nav.css";

const Nav = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((state) => state.userSlice.user);
  const [show, setShow] = useState(false);
  const [activeButton, setActiveButton] = useState(
    parseInt(localStorage.getItem("activeButton")) || 1
  );
  const location = useLocation();
  const isMobile = useMediaQuery({ minWidth: 320, maxWidth: 1024 });

  const handleClickActive = (id) => {
    setActiveButton(id);
    localStorage.setItem("activeButton", id);
  };

  const transitionNavBar = () => {
    if (window.scrollY > 100) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", transitionNavBar);

    const path = location.pathname;
    if (path === "/") {
      setActiveButton(1);
    } else if (path === "/Movies") {
      setActiveButton(2);
    } else if (path === "/Tv") {
      setActiveButton(3);
    } else if (path === "/search") {
      setActiveButton(4);
    }

    const unsub = onAuthStateChanged(auth, (user) => {
      if (user) {
        const { displayName, email, uid } = user;
        dispatch(setCurrentUser({ displayName, uid, email }));
      } else {
        dispatch(setCurrentUser(null));
      }
    });

    return unsub;
  }, [location.pathname]);

  return (
    <div
      className={`fixed z-10 ease-in transition-all-0.25s w-full font-semibold ${
        show && "bg-black"
      }`}
    >
      <div className='nav__contents flex justify-between items-center px-4 h-20'>
        {isMobile ? (
          <Hamburger />
        ) : (
          <div className='flex'>
            <img
              onClick={() => {
                navigate("/");
              }}
              className='w-20 hover:cursor-pointer'
              src='https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png'
              alt='nav logo'
            />
            <button
              type='button'
              className={`${activeButton === 1 ? "active" : ""} nav__button`}
              onClick={() => {
                handleClickActive(1);
                navigate("/");
              }}
            >
              HOME
            </button>
            <button
              type='button'
              className={`${activeButton === 2 ? "active" : ""} nav__button`}
              onClick={() => {
                handleClickActive(2);
                navigate("/Movies");
              }}
            >
              MOVIES
            </button>
            <button
              className={`${activeButton === 3 ? "active" : ""} nav__button`}
              onClick={() => {
                handleClickActive(3);
                navigate("/Tv");
              }}
            >
              TV SERIES
            </button>
            <button
              className={`${activeButton === 4 ? "active" : ""} nav__button`}
              onClick={() => {
                handleClickActive(4);
                navigate("/search");
              }}
            >
              SEARCH
            </button>
          </div>
        )}

        {user ? <UserMenu name={user?.displayName} /> : <SignUpModal />}
      </div>
    </div>
  );
};

export default Nav;
