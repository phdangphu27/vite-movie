import React, { useState, useEffect } from "react";
import { SwiperSlide, Swiper } from "swiper/react";
import { Navigation } from "swiper";
import { movieService } from "../../services/movieService";
import "swiper/css";
import "swiper/css/navigation";
import "./Row.css";
import MovieCard from "./MovieCard";
import useApi from "../../hook/useApi";

const Row = ({ apiEndpoint, category, type }) => {
  const { data } = useApi(apiEndpoint);

  const renderRow = () => {
    return (
      <div className='mb-12 text-white'>
        <h3 className='mb-5 text-xl'>{category.toUpperCase()}</h3>
        <Swiper
          modules={[Navigation]}
          navigation={true}
          slidesPerView={2}
          breakpoints={{
            600: {
              slidesPerView: 3,
            },
            768: {
              slidesPerView: 3,
            },
            1024: {
              slidesPerView: 4,
            },
            1280: {
              slidesPerView: 5,
            },
          }}
        >
          {data?.results.map((movie) => {
            return movie.poster_path ? (
              <SwiperSlide key={movie.id}>
                <MovieCard movie={movie} type={type} />
              </SwiperSlide>
            ) : (
              ""
            );
          })}
        </Swiper>
      </div>
    );
  };

  return <div>{renderRow()}</div>;
};

export default Row;
