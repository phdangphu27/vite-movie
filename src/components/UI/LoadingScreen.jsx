import React from "react";
import { BarLoader } from "react-spinners";

const LoadingScreen = () => {
  return (
    <div className='bg-black h-screen flex justify-center items-center fixed top-0 left-0 w-full z-10'>
      <BarLoader color='#ff0808' height={5} width={150} />
    </div>
  );
};

export default LoadingScreen;
