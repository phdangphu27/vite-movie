import React, { useState } from "react";
import { IoIosMenu } from "react-icons/io";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Drawer } from "antd";

const Hamburger = () => {
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const [activeButton, setActiveButton] = useState(
    parseInt(localStorage.getItem("activeButton")) || 1
  );
  const [open, setOpen] = useState(false);
  const location = useLocation();

  const onChange = (e) => {
    setPlacement(e.target.value);
  };

  const handleClickActive = (id) => {
    setActiveButton(id);
    localStorage.setItem("activeButton", id);
  };

  useEffect(() => {
    const path = location.pathname;
    if (path === "/") {
      setActiveButton(1);
    } else if (path === "/Movies") {
      setActiveButton(2);
    } else if (path === "/Tv") {
      setActiveButton(3);
    } else if (path === "/search") {
      setActiveButton(4);
    }
  }, [location.pathname]);

  return (
    <div>
      <button
        onClick={() => {
          setOpen(true);
        }}
        className='text-white text-3xl'
      >
        {<IoIosMenu />}
      </button>
      <Drawer
        placement={"left"}
        closable={false}
        onClose={() => {
          setOpen(false);
        }}
        width={250}
        open={open}
        key={"left"}
        style={{ backgroundColor: "#363636" }}
      >
        <div className='flex flex-col text-left text-white items-center'>
          <img
            onClick={() => {
              navigate("/");
              setOpen(false);
            }}
            className='w-20 hover:cursor-pointer mb-3'
            src='https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png'
            alt='nav logo'
          />
          <div className='flex flex-col w-full'>
            <h1 className='text-2xl my-3'>MENU</h1>
            <button
              onClick={() => {
                handleClickActive(1);
                navigate("/");
                setOpen(false);
              }}
              className={`${
                activeButton === 1 ? "active" : ""
              } nav__button transition-all duration-200 ease-in-out rounded p-3 my-3 hover:bg-gray-600`}
            >
              HOME
            </button>
            <button
              onClick={() => {
                handleClickActive(2);
                navigate("/movies");
                setOpen(false);
              }}
              className={`${
                activeButton === 2 ? "active" : ""
              } nav__button transition-all duration-200 ease-in-out rounded p-3 my-3 hover:bg-gray-600`}
            >
              MOVIES
            </button>
            <button
              onClick={() => {
                handleClickActive(3);
                navigate("/tv");
                setOpen(false);
              }}
              className={`${
                activeButton === 3 ? "active" : ""
              } nav__button transition-all duration-200 ease-in-out rounded p-3 my-3 hover:bg-gray-600`}
            >
              TV SERIES
            </button>
            <button
              onClick={() => {
                handleClickActive(4);
                navigate("/search");
                setOpen(false);
              }}
              className={`${
                activeButton === 4 ? "active" : ""
              } nav__button transition-all duration-200 ease-in-out rounded p-3 my-3 hover:bg-gray-600`}
            >
              SEARCH
            </button>
          </div>
        </div>
      </Drawer>
    </div>
  );
};

export default Hamburger;
