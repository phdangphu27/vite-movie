import React from "react";
import { FaFacebook, FaInstagram, FaLinkedin } from "react-icons/fa";

const Footer = () => {
  return (
    <div
      className='footer flex flex-col h-24 text-white p-4 items-center justify-around'
      style={{
        backgroundColor: "#363636",
      }}
    >
      <div className='text-lg pb-3'>CONTACT ME</div>
      <div className='text-lg flex'>
        <button
          className='pr-3'
          onClick={() => {
            window.open("https://www.facebook.com/dphus");
          }}
        >
          <FaFacebook style={{ fontSize: "24px" }} />
        </button>
        <button
          className='pr-3'
          onClick={() => {
            window.open("https://www.instagram.com/ph.sandwich/");
          }}
        >
          <FaInstagram style={{ fontSize: "24px" }} />
        </button>
        <button
          onClick={() => {
            window.open("https://www.linkedin.com/in/dang-phu-pham-939718238/");
          }}
        >
          <FaLinkedin style={{ fontSize: "24px" }} />
        </button>
      </div>
    </div>
  );
};

export default Footer;
