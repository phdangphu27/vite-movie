import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import { requests } from "../../Requests";
import "./Banner.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import { Progress } from "antd";
import Tag from "../UI/Tag";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import useApi from "../../hook/useApi";

const Banner = ({ apiEndpoint, type }) => {
  const [genresMovie, setGenresMovie] = useState([]);
  const navigate = useNavigate();
  const isMobile = useMediaQuery({ minWidth: 320, maxWidth: 425 });
  const { data } = useApi(apiEndpoint);
  const url =
    type === "movie" ? requests.fetchGenresMovie : requests.fetchGerensTv;
  const { data: genresData } = useApi(url);

  useEffect(() => {
    if (genresData && genresData.genres) {
      setGenresMovie(genresData.genres);
    }
  }, [genresData]);

  const renderBanner = () => {
    return (
      <Swiper
        loop={true}
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
        slidesPerView={1}
        modules={[Autoplay]}
      >
        {data?.results.map((item, index) => {
          const backgroundImage = `url('https://image.tmdb.org/t/p/original/${item.backdrop_path}')`;
          return (
            <SwiperSlide key={item.id}>
              <div
                className='bg-top bg-no-repeat bg-cover cursor-grab'
                style={{
                  backgroundImage: backgroundImage,
                }}
              >
                <div className='banner__backdrop--faded'></div>
              </div>
              <div className='banner__contents text-white flex flex-col absolute top-16 md:top-28 lg:top-36 md:p-8 lg:px-28 xl:px-44 p-4'>
                <h1 className='font-semibold text-3xl xl:text-5xl'>
                  {item.original_title || item.original_name}
                </h1>
                <div>
                  <div className='my-6'>
                    <Progress
                      type='circle'
                      percent={item.vote_average * 10}
                      format={(percent) => (
                        <div style={{ color: "white" }}>
                          {(percent / 10).toFixed(1)}
                        </div>
                      )}
                      width={50}
                      strokeColor='green'
                      strokeWidth={10}
                    />
                    {[...item.genre_ids].splice(0, 2).map((genreId) => {
                      return (
                        <Tag color='red' key={genreId}>
                          {
                            genresMovie.find((genre) => genre.id === genreId)
                              ?.name
                          }
                        </Tag>
                      );
                    })}
                  </div>
                </div>
                <h1 className='banner__overview text-base text-gray-300 my-6'>
                  {isMobile
                    ? item.overview.slice(0, 100) + "..."
                    : item.overview}
                </h1>
                <button
                  className='bg-red-700 text-white font-medium py-2 px-4 rounded w-min my-6'
                  onClick={() => {
                    navigate(`/${type}/${item.id}`);
                  }}
                >
                  TRAILER
                </button>
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    );
  };

  return <div>{renderBanner()}</div>;
};

export default Banner;
