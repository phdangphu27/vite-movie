import { Modal } from "antd";
import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import "./SignUpModal.css";
import {
  setShow,
  signIn,
  signUp,
  setError,
  resetUserMessageAndError,
  resetPassword,
} from "../../slice/userSlice";
import { useDispatch, useSelector } from "react-redux";

const SignUpModal = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.userSlice.show);
  const [activeForm, setActiveForm] = useState(1);

  const showModal = () => {
    dispatch(setShow(true));
  };

  const handleCancel = () => {
    formik.resetForm();
    dispatch(setError(null));
    dispatch(setShow(false));
  };

  const signInSchema = Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 characters")
      .required("Password is required"),
  });

  const signUpSchema = Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    displayName: Yup.string().max(20).required("Display name is required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 characters")
      .required("Password is required"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Passwords must match")
      .required("Confirm password is required"),
  });

  const resetSchema = Yup.object().shape({
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
  });

  const getCurrentValidationSchema = () => {
    if (activeForm === 1) {
      return signInSchema;
    } else if (activeForm === 2) {
      return signUpSchema;
    } else {
      return resetSchema;
    }
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      displayName: "",
      confirmPassword: "",
    },

    validationSchema: getCurrentValidationSchema(),

    onSubmit: ({ email, password, displayName }) => {
      if (activeForm === 1) {
        dispatch(signIn({ email, password }));
      } else if (activeForm === 2) {
        dispatch(signUp({ email, password, displayName }));
      } else {
        dispatch(resetPassword(email));
      }
    },
  });

  return (
    <>
      <button
        onClick={showModal}
        className='bg-red-700 text-white font-medium py-2 px-4 rounded'
      >
        SIGN IN
      </button>
      <Modal
        className='flex flex-col px-4'
        open={show}
        onCancel={handleCancel}
        footer={null}
      >
        <div className='text-center text-2xl mb-5 text-white flex justify-center'>
          <img
            className='w-20'
            src='https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png'
            alt='nav logo'
          />
        </div>
        {activeForm === 1 && (
          <form onSubmit={formik.handleSubmit} className='signUpForm'>
            <div className='text-lg mb-3 formItem'>
              {formik.touched.email && formik.errors.email ? (
                <div style={{ color: "red" }}>{formik.errors.email}</div>
              ) : (
                <div>Email</div>
              )}
              <input
                id='email'
                name='email'
                type='email'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />
            </div>

            <div className='text-lg mb-3 formItem'>
              {formik.touched.password && formik.errors.password ? (
                <div style={{ color: "red" }}>{formik.errors.password}</div>
              ) : (
                <div>Password</div>
              )}

              <input
                id='password'
                name='password'
                type='password'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
              />
            </div>

            <div className='flex flex-col'>
              <button
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
                type='submit'
              >
                SIGN IN
              </button>
              <button
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
                type='button'
                onClick={() => {
                  setActiveForm(2);
                  dispatch(resetUserMessageAndError(null));
                }}
              >
                SIGN UP
              </button>
              <button
                type='button'
                onClick={() => {
                  setActiveForm(3);
                  dispatch(resetUserMessageAndError(null));
                }}
                className='p-3'
              >
                Forgot your password?
              </button>
            </div>
          </form>
        )}
        {activeForm === 2 && (
          <form onSubmit={formik.handleSubmit} className='signUpForm'>
            <div className='text-lg mb-3 formItem'>
              {formik.touched.email && formik.errors.email ? (
                <div style={{ color: "red" }}>{formik.errors.email}</div>
              ) : (
                <div>Email</div>
              )}
              <input
                id='email'
                name='email'
                type='email'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />
            </div>

            <div className='text-lg mb-3 formItem'>
              {formik.touched.displayName && formik.errors.displayName ? (
                <div style={{ color: "red" }}>{formik.errors.displayName}</div>
              ) : (
                <div>Display Name</div>
              )}
              <input
                id='displayName'
                name='displayName'
                type='displayName'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.displayName}
              />
            </div>

            <div className='text-lg mb-3 formItem'>
              {formik.touched.password && formik.errors.password ? (
                <div style={{ color: "red" }}>{formik.errors.password}</div>
              ) : (
                <div>Password</div>
              )}

              <input
                id='password'
                name='password'
                type='password'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
              />
            </div>

            <div className='text-lg mb-3 formItem'>
              {formik.touched.confirmPassword &&
              formik.errors.confirmPassword ? (
                <div style={{ color: "red" }}>
                  {formik.errors.confirmPassword}
                </div>
              ) : (
                <div>Confirm Password</div>
              )}

              <input
                id='confirmPassword'
                name='confirmPassword'
                type='password'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.confirmPassword}
              />
            </div>

            <div className='flex flex-col'>
              <button
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
                type='submit'
              >
                SIGN UP
              </button>
              <button
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
                type='button'
                onClick={() => {
                  setActiveForm(1);
                  dispatch(resetUserMessageAndError(null));
                }}
              >
                SIGN IN
              </button>
            </div>
          </form>
        )}
        {activeForm === 3 && (
          <form
            onSubmit={formik.handleSubmit}
            className='signUpForm flex flex-col'
          >
            <div className='text-lg mb-3 formItem'>
              {formik.touched.email && formik.errors.email ? (
                <div style={{ color: "red" }}>{formik.errors.email}</div>
              ) : (
                <div>Email</div>
              )}
              <input
                id='email'
                name='email'
                type='email'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
              />
            </div>
            <div className='flex flex-col'>
              <button
                type='submit'
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
              >
                Reset my password
              </button>
              <button
                type='button'
                onClick={() => {
                  setActiveForm(1);
                  dispatch(resetUserMessageAndError(null));
                }}
                className='p-3 mt-1 bg-red-600 rounded text-white transition-all cursor-pointer hover:bg-red-950'
              >
                SIGN IN
              </button>
            </div>
          </form>
        )}
      </Modal>
    </>
  );
};

export default SignUpModal;
