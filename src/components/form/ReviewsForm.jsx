import React from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { updateDoc, addDoc, setDoc, doc, arrayUnion } from "firebase/firestore";
import { db } from "../../firestore";
import { useSelector } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";

const ReviewsForm = ({ id, type }) => {
  const currentUser = useSelector((state) => state.userSlice.user);
  const docRef = doc(db, "comments", type);

  const reviewsSchema = Yup.object().shape({
    comment: Yup.string().required("Comment can't be empty"),
  });

  const reviewsFormik = useFormik({
    initialValues: {
      comment: "",
    },

    validationSchema: reviewsSchema,

    onSubmit: (values, { resetForm }) => {
      const cid = uuidv4();
      const createdAt = moment().format("DD-MM-YY hh:mm:ss");
      updateDoc(
        docRef,
        {
          [`${id}`]: arrayUnion({
            uid: currentUser.uid,
            content: values.comment,
            displayName: currentUser.displayName,
            cid,
            createdAt,
          }),
        },
        { merge: true }
      )
        .then(() => {
          console.log("success");
          resetForm();
        })
        .catch((err) => {
          console.log(err);
        });
    },
  });

  return (
    <form onSubmit={reviewsFormik.handleSubmit}>
      <div className='formItem py-4'>
        {reviewsFormik.touched.comment && reviewsFormik.errors.comment ? (
          <div style={{ color: "red" }}>{reviewsFormik.errors.comment}</div>
        ) : (
          ""
        )}
        <textarea
          rows={4}
          style={{ resize: "none" }}
          className='reviewsForm_input w-full bg-black border-2 rounded p-3'
          placeholder='Write your review'
          id='comment'
          name='comment'
          type='comment'
          onChange={reviewsFormik.handleChange}
          onBlur={reviewsFormik.handleBlur}
          value={reviewsFormik.values.comment}
        />
      </div>
      <div className='py-4'>
        <button className='formButton rounded p-3 bg-red-600'>
          POST COMMENT
        </button>
      </div>
    </form>
  );
};

export default ReviewsForm;
