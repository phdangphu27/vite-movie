import React, { useState } from "react";
import { Modal, Menu } from "antd";

const MenuListModal = () => {
  const [visible, setVibible] = useState(false);

  const handleOpenModal = () => {
    setVibible(true);
  };

  const handleCancelModal = () => {
    setVibible(false);
  };

  return (
    <div>
      <button onClick={handleOpenModal}>Open modal</button>
      <Modal open={visible} onCancel={handleCancelModal} footer={null}>
        <Menu>
          <Menu.Item>test</Menu.Item>
        </Menu>
      </Modal>
    </div>
  );
};

export default MenuListModal;
