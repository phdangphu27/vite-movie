import { initializeApp } from "firebase/app";
import { getDocs, getFirestore } from "firebase/firestore";
import {
  browserLocalPersistence,
  getAuth,
  setPersistence,
} from "firebase/auth";
import { collection, setDoc, addDoc, doc } from "firebase/firestore";
import { useSelector } from "react-redux";
import { onSnapshot } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAMljHpVZgUF2B00OyT2bhp74BheM3xL5Y",
  authDomain: "vite-movie.firebaseapp.com",
  projectId: "vite-movie",
  storageBucket: "vite-movie.appspot.com",
  messagingSenderId: "244513926054",
  appId: "1:244513926054:web:534ec1a4121c3531fdfd67",
  measurementId: "G-038MQ2YWFQ",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db = getFirestore();

export const auth = getAuth();
